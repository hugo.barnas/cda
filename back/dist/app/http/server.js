"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = void 0;
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const config_1 = require("./../../config");
const config_2 = require("./../../config");
const router_1 = require("../../router");
console.log("v1Router :", router_1.v1Router);
const createServer = () => __awaiter(void 0, void 0, void 0, function* () {
    const server = (0, express_1.default)();
    server.use((0, cors_1.default)({
        origin: "http://localhost:3000",
    }));
    server.use(config_2.APP_BASE_URL, router_1.v1Router);
    server.listen(config_1.PORT, () => {
        console.log(`[App]: Server is now running in PORT ${config_1.PORT} in ${config_1.NODE_ENV} mode`);
    });
    return server;
});
exports.createServer = createServer;
//# sourceMappingURL=server.js.map