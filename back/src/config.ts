import dotenv from "dotenv";
import path from "path";

const envPath = path.join(__dirname, "../../");
dotenv.config({ path: envPath + "./.env" });

export const PORT = process.env.PORT;
export const NODE_ENV = process.env.NODE_ENV;
export const APP_BASE_URL = process.env.APP_BASE_URL || "/api/v1/";
