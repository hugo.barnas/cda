import { Router, Request, Response } from "express";

export const v1Router: Router = Router();

v1Router.get("/spectacles", (_: Request, response: Response) => {
  response.json("Liste des spectacles");
});

v1Router.get("/", (_: Request, response: Response) => {
  response.json("Racine de mon api !");
});
