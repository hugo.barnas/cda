import express from "express";
import cors from "cors";
import { PORT, NODE_ENV } from "./../../config";
import { APP_BASE_URL } from "./../../config";

import { v1Router } from "../../router";
console.log("v1Router :", v1Router);

export const createServer = async (): Promise<express.Application> => {
  const server: express.Application = express();

  server.use(
    cors({
      origin: "http://localhost:3000",
    })
  );

  server.use(APP_BASE_URL, v1Router);

  server.listen(PORT, () => {
    console.log(
      `[App]: Server is now running in PORT ${PORT} in ${NODE_ENV} mode`
    );
  });

  return server;
};
